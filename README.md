# ATLAS LAr Integration Week <br> Brookhaven National Laboratory <br> </span> June 10-14, 2024 <br>


## Visiting Brookhaven

Brookhaven is one of the U.S. Department of Energy's multidisciplinary research laboratories, and is dedicated to basic, non-defense scientific research. You'll often hear the Department of Energy referred to as DOE. Brookhaven is operated by Brookhaven Science Associates (BSA), a not-for-profit research company founded by the Battelle Memorial Institute and the Research Foundation of the State University of New York on behalf of the State University of New York at Stony Brook. About 3,000 people work here, and more than 4,000 others visit each year for days, weeks, or months to perform scientific research or attend conferences. To learn more about what the Laboratory is all about, explore the Brookhaven homepage.

One of the strongest recommendations anyone can make about coming to Brookhaven for a visitor is to bring a car. The Lab has many amenities, but not everything you'll need, and the nearest stores and towns are several miles away. Please note that the inclusion of a commercial establishment such as a restaurant doesn't indicate an endorsement by the Laboratory.

<img src="li_map.gif" alt="drawing" width="600"/>

## Location & Directions
Brookhaven National Laboratory
Upton, NY 11973 USA
[Directions](https://www.bnl.gov/maps/)

Participants will meet at Physics Department (Bldg. 510) electronics lab room 1-203 and seminor room 1-106. [Directions](https://www.bnl.gov/maps/510map.php)


## General Information

[About Brookhaven](https://www.bnl.gov/about/)<br>
[Airport Limo Services](https://www.bnl.gov/staffservices/airportshuttle.php)<br>
[BNL Visitor's Guide](https://www.bnl.gov/visitorinfo/)<br>
[BNL Visitor's Information](https://www.bnl.gov/visiting/)<br>
[Onsite Services at Brookhaven](https://www.bnl.gov/staffservices/)<br>
[Food at Brookhaven](https://www.bnl.gov/staffservices/foodservices.php)<br>
[Transportation Services](https://www.bnl.gov/staffservices/othertransportation.phpurl)<br>
[Maps & Directions to Brookhaven](https://www.bnl.gov/maps/)<br>

## Visa Information

Participants should check if they require a visa to enter the United States on the U.S. Department of State's [U.S. Visas](https://travel.state.gov/content/travel/en/us-visas.html) website.

Foreign travelers, who are citizens from certain eligible countries, may also be able to visit the U.S. for business (e.g., attend a scientific conference) or tourism for 90 days or less without a visa if they meet the [Visa Waiver Program](https://travel.state.gov/content/travel/en/us-visas/tourism-visit/visa-waiver-program.html) requirements. All eligible travelers who wish to travel under the Visa Waiver Program must apply for authorization online through [ESTA (Electronic System for Travel Authorization)](https://esta.cbp.dhs.gov/).

If you require a visa to enter the U.S., we strongly recommend that you start the visa application process as soon as possible. View [visa appointment and processing wait times](https://travel.state.gov/content/travel/en/us-visas/visa-information-resources/wait-times.html).

[Contact us](haoxu@bnl.gov) to request invitation letter for your visa applicaation. 

## Access to Brookhaven Lab
Non-U.S. citizens must complete the [Brookhaven Lab Guest Form](https://www.bnl.gov/usfccworkshop/gisinstr.php) and be approved in the Guest Information System (GIS). We suggest you complete the registration early since it will take weeks to get approval.

U.S. citizens who are planning to use BNL's onsite accommodations must also complete the additional Brookhaven Lab Guest Form and be approved in the BNL Guest Information System (GIS).

Host information for the Brookhaven Lab Guest Form<br>
[Michael Begel](https://www.bnl.gov/staff/begel)<br>
email: [begel@bnl.gov](begel@bnl.gov)<br>
Tel: (631) 344-3403<br>
Senior Physicist<br>
Physics Department<br>

## Internet & WiFi Access
Registration of Guest's Computing Devices - If guests are bring laptops, tablets, smartphones, etc. that will require internet access, then these devices must be registered in Brookhaven National Lab's (BNL) Network Access Registration database in order to access the internet at BNL.

Registering your computer device can be done via the device's web browser upon first connecting to Brookhaven Lab's network. The instructions are availabe [here](https://www.bnl.gov/itd/networking/).

## Nearby Hotels
While the immediate vicinity around BNL has no large hotels, there are many establishments within a 45 minute drive of the Laboratory. [View Listing](https://www.bnl.gov/visitorinfo/hotels.php#NearBNL)
